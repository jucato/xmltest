#-------------------------------------------------
#
# Project created by QtCreator 2016-07-19T19:42:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets quick

TARGET = xmltest
TEMPLATE = app


SOURCES += main.cpp \
    pagemodel.cpp \
    page.cpp \
    notebookmodel.cpp

HEADERS  += \
    pagemodel.h \
    page.h \
    notebookmodel.h

DISTFILES += \
    view.qml \
    ImageItem.qml \
    TextItem.qml \
    res/kde.png \
    res/view.qml \
    res/basket-3.xml \
     res/text.qml \
    res/image.qml \
    res/basket-1.xml \
    res/baskets.xml

copydata.commands = $(COPY_DIR) $$PWD/res $$OUT_PWD
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata
