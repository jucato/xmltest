#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QtQuick/QQuickView>
#include <QtQml/QQmlContext>
#include <QDebug>

//#include "pagemodel.h"
#include "page.h"

int main(int argc, char *argv[])
{
    QGuiApplication a(argc, argv);

//    pageModelModel pageModel;
//    pageModel.addpageModelItem(10, 10, 96, 96, "image", QUrl("/usr/share/icons/oxygen/base/48x48/apps/kde.png"));
//    pageModel.addpageModelItem(300, 10, 400, DEFAULT_HEIGHT, "image", QUrl("/usr/share/icons/oxygen/base/48x48/apps/kde.png"));
//    pageModel.addpageModelItem(50, 200, 320, 320, "text");
//    pageModel.addpageModelItem(400, 150, DEFAULT_WIDTH, DEFAULT_HEIGHT, "image", QUrl("/usr/share/icons/oxygen/base/48x48/apps/kde.png"));
//    pageModel.addpageModelItem(10, 400, DEFAULT_WIDTH, DEFAULT_HEIGHT, "text");

    Page page("./res/basket-1.xml");
    page.load();

//    QQuickView view;
//    view.setResizeMode(QQuickView::SizeRootObjectToView);
//    QQmlContext *ctxt = view.rootContext();
//    ctxt->setContextProperty("PageModel", &pageModel);

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("PageModel", page.model());
    engine.load(QUrl("./res/view.qml"));

//    view.setSource(QUrl("./view.qml"));
//    view.show();

    return a.exec();
}
