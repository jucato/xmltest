import QtQuick 2.0
import QtQuick.Window 2.0
import QtQuick.XmlListModel 2.0

Window {
    visible: true
    id: root
    width: 1024
    height: 768

    Rectangle {
        id: list
        width: 360
        anchors { right: parent.right; bottom: parent.bottom; top: parent.top }
        color: "lightsteelblue"

        Column {
            id: actions
            anchors { right: parent.right; left: parent.left; top: parent.top }

            Row {
                height: 24
                anchors { right: parent.right; left: parent.left }
                spacing: 24

                Text {
                    text: "Row: "
                }

                Rectangle {
                    color: "white"
                    width: parent.height;
                    height: parent.height
                    TextInput {
                        id: row
                        anchors.fill: parent
                        text: listview.count - 1
                    }
                }

                Text {
                    text: "Num items: "
                }

                Rectangle {
                    color: "white"
                    width: parent.height;
                    height: parent.height
                    TextInput {
                        id: numItems
                        anchors.fill: parent
                        text: "1"
                    }
                }

                Rectangle {
                    id: removeButton
                    color: "red"
                    width: 128
                    height: parent.height
                    Text { anchors.centerIn: parent; text: "Remove items" }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: listview.model.removePageItems(row.text, numItems.text);
                    }
                }
            }

            Row {
                height: 24
                anchors { right: parent.right; left: parent.left; }
                spacing: 24

                Text {
                    text: "X: "
                }

                Rectangle {
                    color: "white"
                    width: parent.height;
                    height: parent.height
                    TextInput {
                        id: newX
                        anchors.fill: parent
                        text: "1"
                    }
                }

                Text {
                    text: "Y: "
                }

                Rectangle {
                    color: "white"
                    width: parent.height;
                    height: parent.height
                    TextInput {
                        id: newY
                        anchors.fill: parent
                        text: "1"
                    }
                }

                Rectangle {
                    id: button
                    color: "red"
                    width: 128
                    height: parent.height
                    Text { anchors.centerIn: parent; text: "Change X, Y" }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: listview.model.changePageItemXY(row.text, newX.text, newY.text)
                    }
                }
            }
        }

        ListView {
            id: listview
            anchors { right: parent.right; bottom: parent.bottom; left: parent.left; top: actions.bottom }
            model: PageModel
            delegate: Text { text: "#" + index + ": " +
                    "(" + model.x + ", " + model.y + ") - [" + model.width + ", " + model.height + "]\n" +
                    model.type + " - " + model.filename + "\n" +
                    "created: " + model.dateCreated + " (modified: " + model.dateModified + ")\n" +
                    "tags: " + model.tags + "\n----------------"
            }
        }
    }

    Rectangle {
        id: canvas
        anchors { left: parent.left; bottom: parent.bottom; right: list.left; top: parent.top }


        Repeater {
            id: pageview
            model: PageModel
            delegate: Loader {
                id: itemLoader

                source: model.type + ".qml"
            }
        }
    }
}
