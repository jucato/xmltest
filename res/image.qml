import QtQuick 2.0

Rectangle {
    id: image
    x: model.x
    y: model.y
    width: model.width
    height: model.height
    border.width: 1
    border.color: "black"

    Image {
//        source: model.filename
        source: "kde.png"
        fillMode: Image.PreserveAspectFit
        anchors.fill: parent
    }

    MouseArea {
        id: dragArea
        anchors.fill: parent
        drag.target: parent
    }

    Drag.active: dragArea.drag.active
    Drag.onActiveChanged: {
        if (!Drag.active) {
            PageModel.changePageItemXY(model.index, x, y);
        }
    }
}
