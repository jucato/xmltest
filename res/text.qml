import QtQuick 2.0
import QtQuick.Controls 1.4

TextArea {
    id: textEdit
    x: model.x
    y: model.y
//    width: model.width
//    height: model.height
    height: contentHeight

    property int maxWidth: font.pointSize * 10

//    width: Math.max(contentWidth, maxWidth)

    font.family: "Arial"
    font.pointSize: 20

    textFormat: TextEdit.AutoText
    text: "<b>Hello</b> <i>World!</i><br /><a href=\"http://jucato.logbert.org\">http://jucato.logbert.org</a><br />An editable text area"
    wrapMode: TextEdit.Wrap
    focus: true
    verticalScrollBarPolicy: Qt.ScrollBarAlwaysOff
    backgroundVisible: false

    onLinkActivated: Qt.openUrlExternally(link)
}
