#include "page.h"

#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QFile>
#include <QDateTime>
#include <QDebug>

Page::Page(QString filename, QQuickItem* parent) : QQuickItem(parent)
{
    m_pageModel = new PageModel(this);

    if (m_filename != filename && !filename.isNull() && !filename.isEmpty())
        setFilename(filename);
}

bool Page::load(QString filename)
{
    if (m_filename != filename && !filename.isNull() && !filename.isEmpty())
        setFilename(filename);

    QXmlStreamReader xmlReader;

    QFile file(m_filename);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << "Error loading file: " << m_filename;
        return false;
    }

    xmlReader.setDevice(&file);

    PageItem pageItem;

    while (!xmlReader.atEnd())
    {
        if (xmlReader.isStartElement())
        {
            QString tagName = xmlReader.name().toString();

            if (tagName == "note")
            {
                pageItem.x = 0;
                pageItem.y = 0;
                pageItem.width = DEFAULT_WIDTH;
                pageItem.height = DEFAULT_HEIGHT;

                for (int i = 0; i < xmlReader.attributes().size(); i++)
                {
                    QXmlStreamAttribute attribute = xmlReader.attributes().at(i);

                    if (attribute.name() == "type")
                        pageItem.type = attribute.value().toString();
                    else if (attribute.name() == "x")
                        pageItem.x = attribute.value().toInt();
                    else if (attribute.name() == "y")
                        pageItem.y = attribute.value().toInt();
                    else if (attribute.name() == "width")
                        pageItem.width = attribute.value().toInt();
                    else if (attribute.name() == "height")
                        pageItem.height = attribute.value().toInt();
//                    else if (attribute.name() == "dateCreated")
//                        pageItem.dateCreated = QDateTime::fromString(attribute.value().toString(), "YYYY-MM-dd'T'hh:mm:ss");
//                    else if (attribute.name() == "dateModified")
//                        pageItem.dateModified = QDateTime::fromString(attribute.value().toString(), "YYYY-MM-DDTHH:mm:ss");
                }
            }

            if (tagName == "filename")
            {
                QString filename = xmlReader.readElementText();
//                qDebug() << tagName << filename;
                pageItem.filename = filename;
            }

            if (tagName == "tags")
            {
                QStringList tags = xmlReader.readElementText().split(";");
//                qDebug() << tagName << tags;
                pageItem.tags = tags;
            }
        }

        if (xmlReader.isEndElement())
        {
            QString tagName = xmlReader.name().toString();

            if (tagName == "note")
//                qDebug() << "======================";

            m_pageModel->addPageItem(&pageItem);
        }

        xmlReader.readNext();
    }

    if (xmlReader.hasError())
    {
        qDebug() << "Error loading XML file: " << xmlReader.errorString();
        return false;
    }

    m_pageModel->printPageItems();
    return true;
}

bool Page::save(void)
{
    return true;
}

void Page::setFilename(QString filename)
{
    if (m_filename != filename && !filename.isNull() && !filename.isEmpty())
        m_filename = filename;

    emit filenameChanged(m_filename);
}

void Page::setModel(PageModel* model)
{
    if (!model && m_pageModel != model)
        m_pageModel = model;

    emit modelChanged();
}
