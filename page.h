#ifndef PAGE_H
#define PAGE_H

#include <QQuickItem>

#include "pagemodel.h"

class Page : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QString filename READ filename WRITE setFilename NOTIFY filenameChanged)
    Q_PROPERTY(PageModel* model READ model WRITE setModel NOTIFY modelChanged)

    public:
        Page(QString filename = QString(), QQuickItem* parent = 0);

        const QString filename(void) const { return m_filename; }

        PageModel* model(void) { return m_pageModel; }
        const PageModel* model(void) const { return m_pageModel; }


    signals:
        void filenameChanged(QString filename);
        void modelChanged(void);

    public slots:
        bool load(QString filename = QString());
        bool save(void);

        void setFilename(QString filename);
        void setModel(PageModel* model);

    private:
        PageModel* m_pageModel;
        QString m_filename;
};

#endif // PAGE_H
