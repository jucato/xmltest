#ifndef PAGEMODEL_H
#define PAGEMODEL_H

#include <QAbstractListModel>
#include <QVector>
#include <QHash>
#include <QString>
#include <QStringList>
#include <QDateTime>

const int DEFAULT_WIDTH = 200;
const int DEFAULT_HEIGHT = 150;
const QString DEFAULT_TYPE = "text";

struct PageItem
{
    int x;
    int y;
    int width;
    int height;
    QString type;
    QString filename;
    QDateTime dateCreated;
    QDateTime dateModified;
    QStringList tags;
};

class PageModel : public QAbstractListModel
{
    Q_OBJECT

    public:
        explicit PageModel(QObject *parent = 0);

        enum PageItemProps { XProperty = Qt::UserRole + 1, YProperty, WidthProperty, HeightProperty, TypeProperty,
                             FilenameProperty, DateCreatedProperty, DateModifiedProperty, TagsProperty };

        // Header:
        // QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

        // bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;

        // Basic functionality:
        int rowCount(const QModelIndex &parent = QModelIndex()) const override;

        QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

        // Editable:
        bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

        Qt::ItemFlags flags(const QModelIndex& index) const override;


        // Remove data:
        bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

        // TODO: Implement Mime Data
        void printPageItems(void);

    public slots:
        void addPageItem(PageItem* item);
        void addPageItem(int x = 0, int y = 0, int width = DEFAULT_WIDTH, int height = DEFAULT_HEIGHT, QString type = DEFAULT_TYPE, QString filename = QString(),
                         QDateTime dateCreated = QDateTime::currentDateTime(), QDateTime dateModified = QDateTime::currentDateTime(), QStringList tags = QStringList());
        void addPageItems(QVector<PageItem*> items);

        void removePageItems(int row, int count);

        void changePageItemXY(int row, int x, int y);

    protected:
        QHash<int, QByteArray> roleNames() const;


    private:
        QVector<PageItem> m_pageItems;
};

#endif // PAGEMODEL_H
