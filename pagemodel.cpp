#include "pagemodel.h"

#include <QDebug>

PageModel::PageModel(QObject *parent)
    : QAbstractListModel(parent)
{
    m_pageItems.clear();
}

/*
QVariant PageModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    // FIXME: Implement me!
    return QVariant();
}

bool PageModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    if (value != headerData(section, orientation, role)) {
        // FIXME: Implement me!
        emit headerDataChanged(orientation, section, section);
        return true;
    }

    return false;
}
*/

int PageModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return m_pageItems.size();
}

QVariant PageModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() < 0 || index.row() >= m_pageItems.size())
        return QVariant();

    if (role == XProperty)
        return m_pageItems.at(index.row()).x;
    else if (role == YProperty)
        return m_pageItems.at(index.row()).y;
    else if (role == WidthProperty)
        return m_pageItems.at(index.row()).width;
    else if (role == HeightProperty)
        return m_pageItems.at(index.row()).height;
    else if (role == TypeProperty)
        return m_pageItems.at(index.row()).type;
    else if (role == FilenameProperty)
        return m_pageItems.at(index.row()).filename;
    else if (role == DateCreatedProperty)
        return m_pageItems.at(index.row()).dateCreated;
    else if (role == DateModifiedProperty)
        return m_pageItems.at(index.row()).dateModified;
    else if (role == TagsProperty)
        return m_pageItems.at(index.row()).tags;

    return QVariant();
}

bool PageModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value) {

        if (role == XProperty)
            m_pageItems[index.row()].x = value.toInt();
        else if (role == YProperty)
            m_pageItems[index.row()].y = value.toInt();
        else if (role == WidthProperty)
            m_pageItems[index.row()].width = value.toInt();
        else if (role == HeightProperty)
            m_pageItems[index.row()].height = value.toInt();
        else if (role == TypeProperty)
            m_pageItems[index.row()].type = value.toString();
        else if (role == FilenameProperty)
            m_pageItems[index.row()].filename = value.toString();
        else if (role == DateCreatedProperty)
            m_pageItems[index.row()].dateCreated = value.toDateTime();
        else if (role == DateModifiedProperty)
            m_pageItems[index.row()].dateModified = value.toDateTime();
        else if (role == TagsProperty)
            m_pageItems[index.row()].tags = value.toStringList();

        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }

    return false;
}

Qt::ItemFlags PageModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return (QAbstractListModel::flags(index) | Qt::ItemIsEditable | Qt::ItemIsDragEnabled);
}

QHash<int, QByteArray> PageModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[XProperty] = "x";
    roles[YProperty] = "y";
    roles[WidthProperty] = "width";
    roles[HeightProperty] = "height";
    roles[TypeProperty] = "type";
    roles[FilenameProperty] = "filename";
    roles[DateCreatedProperty] = "dateCreated";
    roles[DateModifiedProperty] = "dateModified";
    roles[TagsProperty] = "tags";

    return roles;
}

bool PageModel::removeRows(int row, int count, const QModelIndex &parent)
{
    if (row < 0 || row >= m_pageItems.size() || (row + count) > m_pageItems.size())
        return false;

    beginRemoveRows(parent, row, row + count - 1);
    m_pageItems.remove(row, count);
    endRemoveRows();

    return true;
}

void PageModel::addPageItem(PageItem* item)
{
    PageItem newItem;
    newItem.x = item->x;
    newItem.y = item->y;
    newItem.width = item->width;
    newItem.height = item->height;
    newItem.type = item->type;
    newItem.filename = item->filename;
    newItem.dateCreated = item->dateCreated;
    newItem.dateModified = item->dateModified;
    newItem.tags = item->tags;

    beginInsertRows(QModelIndex(), m_pageItems.size(), m_pageItems.size());
    m_pageItems.append(newItem);
    endInsertRows();
}

void PageModel::addPageItem(int x, int y, int width, int height, QString type, QString filename, QDateTime dateCreated, QDateTime dateModified, QStringList tags)
{
    PageItem newItem;
    newItem.x = x;
    newItem.y = y;
    newItem.width = width;
    newItem.height = height;
    newItem.type = type;
    newItem.filename = filename;
    newItem.dateCreated = dateCreated;
    newItem.dateModified = dateModified;
    newItem.tags = tags;

    addPageItem(&newItem);
}

void PageModel::addPageItems(QVector<PageItem*> items)
{
    for (auto newItem : items)
        addPageItem(newItem);
}

void PageModel::removePageItems(int row, int count)
{
    if (row < 0 || row >= m_pageItems.size() || (row + count) > m_pageItems.size())
        return;

    removeRows(row, count);
}

void PageModel::printPageItems(void)
{
    for (PageItem item : m_pageItems)
        qDebug() << "x: " << item.x << ", y: " << item.y << "\twidth: " << item.width << ", height: " << item.height
                  << "\ntype: " << item.type << "\tfilename: " << item.filename
                  << "\nDate Created: " << item.dateCreated << "\tDate Modified: " << item.dateModified
                  << "\nTags: " << item.tags << "\n";
}

void PageModel::changePageItemXY(int row, int x, int y)
{
    if (row < 0 || row >= m_pageItems.size())
        return;

    setData(index(row, 0), x, XProperty);
    setData(index(row, 0), y, YProperty);
}
